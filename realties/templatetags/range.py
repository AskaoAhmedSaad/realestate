from django.template import Library
register = Library()

@register.filter
def range(value):
	# make paggination pages tabs due to amount of pages
    v = value + 1
    return xrange(1,v)
