# -*- coding:utf-8 -*-
from django.shortcuts import render, Http404, HttpResponseRedirect, get_object_or_404, HttpResponse
from .models import Realty, City, RealtyType, Overlooking, Floor, RealtyStage, Rate, footerEntry
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from patterns.models import Pattern
from sliders.models import Slider
from django.db.models import Q
import json


# Create your views here.
def home(request):
    statistic_cities = City.objects.filter(in_statistic_block=True).order_by('id')[:2]
    saleRealties = Realty.objects.filter(purpose=1)
    buyRealties = Realty.objects.filter(purpose=2)
    realtiesss = Realty.objects.all().order_by('-timestamp')
    cities = City.objects.all()
    types = RealtyType.objects.all()
    patterns = Pattern.objects.all()
    overlookings = Overlooking.objects.all()
    stages = RealtyStage.objects.all()
    if request.method == 'POST':
        if request.POST['type'] != '':
            try:
                realtiesss = realtiesss.filter(typee__name=request.POST['type'])
            except:
                realtiesss = Realty.objects.all()

        if request.POST['status'] != '':
            try:
                realtiesss = realtiesss.filter(purpose=request.POST['status'])
            except:
                realtiesss = Realty.objects.all()

        if request.POST['city'] != '':
            try:
                realtiesss = realtiesss.filter(city__name=request.POST['city'])
            except:
                realtiesss = Realty.objects.all()

        if request.POST['arrange'] != '':
            if request.POST['arrange'] == '1':
                realtiesss = Realty.objects.all().order_by('timestamp')
            elif request.POST['arrange'] == '2':
                realtiesss = Realty.objects.all().order_by('price')

    paginator = Paginator(realtiesss, 9)  # Show 4 contacts per page
    page = request.GET.get('page')
    try:
        realtiesss = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        realtiesss = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        realtiesss = paginator.page(paginator.num_pages)

    sliders = Slider.objects.all()[:5]
    context = {
        'realtiesss': realtiesss,
        'cities': cities,
        'types': types,
        'patterns': patterns,
        'overlookings': overlookings,
        'stages': stages,
        'sliders': sliders,
        'statistic_cities': statistic_cities,
        'saleRealties': saleRealties,
        'buyRealties': buyRealties
    }
    template = "index.html"
    return render(request, template, context)


# def search(request):
#     # later when knowing the search fileds
#     if request.method == 'GET':
#         try:
#             city = request.GET['city']
#         except:
#             city = None
#         try:
#             typee = request.GET['type']
#         except:
#             typee = None
#         try:
#             status = request.GET['status']
#         except:
#             status = None
#         realties = Realty.objects.all()
#         if city != '' and city is not None:
#             print city
#             try:
#                 city = City.objects.get(name=city)
#             except:
#                 city = None
#             if city:
#                 realties = realties.filter(city=city)
#         if typee != '' and typee is not None:
#             print typee
#             try:
#                 typee = RealtyType.objects.get(name=typee)
#             except:
#                 typee = None
#             if typee:
#                 realties = realties.filter(typee=typee)
#         if status != '' and status is not None:
#             print status
#             realties = realties.filter(status=status)
#         template = 'search_maps.html'
#         context = {
#             'realties': realties
#         }
#         return render(request, template, context)
#     else:
#         return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def adv_search(request):
    if request.method == 'POST':
        try:
            pattern = request.POST['pattern']
        except:
            pattern = None

        try:
            fromsize = float(request.POST['fromSize'])
            tosize = float(request.POST['toSize'])
        except:
            fromsize = None
            tosize = None

        try:
            fromprice = float(request.POST['fromPrice'])
            toprice = float(request.POST['toPrice'])
        except:
            fromprice = None
            toprice = None

        try:
            finishing = request.POST['finishing']
        except:
            finishing = None

        try:
            stage = request.POST['stage']
        except:
            stage = None


        # all realties
        realties = Realty.objects.all()

        if pattern != '' and pattern is not None:
            realties = realties.filter(pattern__name__icontains=pattern)

        if fromsize != '' and fromsize is not None and tosize != '' and tosize is not None:
            realties = realties.filter(Q(size__gte=fromsize) & Q(size__lte=tosize))

        if fromprice != '' and fromprice is not None and toprice != '' and toprice is not None:
            realties = realties.filter(Q(price__gte=fromprice) & Q(price__lte=toprice))

        if finishing != '' and finishing is not None:
            if finishing == '1':
                realties = realties.filter(Finishing=True)
            elif finishing == '0':
                realties = realties.filter(Finishing=False)

        if stage != '' and stage is not None:
            realties = realties.filter(stage__name__icontains=stage)
        request.session['realties_search'] = []
        for item in realties:
            request.session['realties_search'].append(item)
        paginator = Paginator(realties, 16)
        page = request.GET.get('page')
        try:
            realties = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, return first page
            realties = paginator.page(1)
        except EmptyPage:
            # If page is out of range
            realties = paginator.page(paginator.num_pages)

        template = 'search.html'
        context = {
            'realties': realties
        }
        # print realties.count()
        return render(request, template, context)
    else:
        realties = request.session['realties_search']
        paginator = Paginator(realties, 8)
        page = request.GET.get('page')
        try:
            realties = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, return first page
            realties = paginator.page(1)
        except EmptyPage:
            # If page is out of range
            realties = paginator.page(paginator.num_pages)
        return render(request, 'search.html', {'realties': realties})


def show(request, pk):
    try:
        ip = request.session['visitor_id']
    except:
        ip = request.session['visitor_id'] = request.META.get('REMOTE_ADDR')
    try:
        realty = Realty.objects.get(pk=pk)
        relatedRealties = Realty.objects.filter(city=realty.city).exclude(pk=realty.pk)[:4]
    except:
        raise Http404
    try:
        rated_value_of_realty = realty.rate_set.filter(ip_address=ip)[0].value
    except:
        rated_value_of_realty = 0
    context = {
        'realty': realty,
        'relatedRealties': relatedRealties,
        'rated_value': rated_value_of_realty,
    }
    template = "realty/show.html"
    return render(request, template, context)


def rate(request):
    if request.method == 'POST':
        # check if the ip unique with the realty or not
        ip = request.META.get('REMOTE_ADDR')
        # print ip
        try:
            realty_id = request.POST['realty']
            # print realty_id
            realty = Realty.objects.get(pk=realty_id)
        except:
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        try:
            value = int(request.POST['value'])
        except:
            value = None
        if value > 5 or value < 1:
            value = 5
        try:
            ratee = Rate.objects.filter(ip_address=ip, realty=realty)[0]
        except:
            ratee = None

        if ratee is None:
            # print 'ratee created'
            ratee = Rate()
            ratee.ip_address = ip
            ratee.value = value
            ratee.realty = realty
            ratee.save()
        else:
            # check the rquest is ajax and return json object to the rates
            if request.is_ajax():
                data = {'data': ratee.realty.avearage_rate()}
                json_data = json.dumps(data)
                return HttpResponse(json_data, content_type='application/json')
            else:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

        if request.is_ajax():
            data = {'data': ratee.realty.avearage_rate()}
            json_data = json.dumps(data)
            return HttpResponse(json_data, content_type='application/json')
        else:
            return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def view_city(request, pk):
    try:
        int(pk)
    except:
        return redirect('/')
    city = City.objects.get(pk=pk)
    types = RealtyType.objects.all()
    realties = Realty.objects.filter(city=city).order_by('-timestamp')
    if request.method == 'POST':
        if request.POST['type'] != '':
            try:
                realties = realties.filter(typee__name=request.POST['type'])
            except:
                realties = Realty.objects.all()
    paginator = Paginator(realties, 9)  # Show 4 contacts per page
    page = request.GET.get('page')
    try:
        realties = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        realties = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        realties = paginator.page(paginator.num_pages)
    return render(request, 'city/view_city.html', {'city': city, 'types': types, 'realties': realties})


def code_search(request):
    allrealties = Realty.objects.all()
    if request.method == 'POST':
        try:
            ip = request.session['visitor_id']
        except:
            ip = request.session['visitor_id'] = request.META.get('REMOTE_ADDR')
        try:
            code = int(request.POST['code'])
        except:
            code = 0
        try:
            realty = allrealties.filter(code=code)[0]   
            relatedrealties = Realty.objects.filter(city=realty.city).exclude(pk=realty.pk)[:4]
            # print relatedrealties
        except:
            raise Http404
        try:
            rated_value_of_realty = realty.rate_set.filter(ip_address=ip)[0].value
        except:
            rated_value_of_realty = 0
        context = {
            'realty': realty,
            'relatedrealties': relatedrealties,
            'rated_value': rated_value_of_realty,
        }
        return render(request, 'code_search_result.html', context)
    else:
        return render(request, 'code_search_result.html', {'allrealties': allrealties})


def list_realties(request):
    if request.method == 'GET':
        allrealties = Realty.objects.all()
        paginator = Paginator(allrealties, 1)  # Show 4 contacts per page
        page = request.GET.get('page')
        try:
            allrealties = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            allrealties = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            allrealties = paginator.page(paginator.num_pages)
        return render(request, 'realty/list_realties.html', {'allrealties': allrealties})
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def footer_entries(request):
    entries = footerEntry.objects.filter(active=True)
    return render(request, 'blocks/footer.html', {'entries': entries})


def view_entry(request, pk):
    try:
        int(pk)
    except:
        return redirect('/')
    entry = footerEntry.objects.get(pk=pk)
    relatedRealties = Realty.objects.filter(typee=entry.entryTypee, purpose=entry.purpose,
                                            city=entry.entryCity)
    paginator = Paginator(relatedRealties, 9)  # Show 4 contacts per page
    page = request.GET.get('page')
    try:
        relatedRealties = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        relatedRealties = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        relatedRealties = paginator.page(paginator.num_pages)
    return render(request, 'view_entry.html', {'entry': entry, 'relatedRealties': relatedRealties})


def set_compare_params(request, param):
    # add params list to seesion if not present
    if 'params' not in request.session:
        request.session['params'] = []
    # get params list from session to append on it
    params = request.session['params']
    # check that the param not present in the list
    if param not in params:
        params.append(param)
    # override the list in the sessio again
    request.session['params'] = params
    # print request.session['params']
    # return success json response
    return HttpResponse(json.dumps('success'))


def remove_compare_params(request, param):
    # get params list from session to append on it
    params = request.session['params']
    # check that the param not present in the list
    if param in params:
        params.remove(param)

    # override the list in the sessio again
    request.session['params'] = params
    # print request.session['params']
    # return success json response
    return HttpResponse(json.dumps('removed'))


def compare_realties(request):
    realties = []
    for p in request.session['params']:
        compareItems = Realty.objects.get(pk=p)
        realties.append(compareItems)
    return render(request, 'realties_compare.html', {'realties': realties})


def search_view(request):
    return render(request, 'blocks/searchTemplate.html')
