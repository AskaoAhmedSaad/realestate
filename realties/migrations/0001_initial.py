# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('others', '0001_initial'),
        ('sliders', '0001_initial'),
        ('patterns', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
                ('in_statistic_block', models.BooleanField(default=False, verbose_name=b'\xd8\xb9\xd8\xb1\xd8\xb6 \xd9\x81\xd9\x89 \xd8\xa7\xd9\x84\xd8\xa3\xd8\xad\xd8\xb5\xd8\xa7\xd8\xa6\xd9\x8a\xd8\xa7\xd8\xaa')),
                ('in_navbar', models.BooleanField(default=False, verbose_name=b'\xd8\xb9\xd8\xb1\xd8\xb6 \xd9\x81\xd9\x89 \xd8\xa7\xd9\x84\xd8\xb1\xd8\xa6\xd9\x8a\xd8\xb3\xd9\x8a\xd8\xa9')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Floor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='footerEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('purpose', models.IntegerField(verbose_name=b'\xd8\xa7\xd9\x84\xd8\xba\xd8\xb1\xd8\xb6', choices=[(1, b'\xd8\xa8\xd9\x8a\xd8\xb9'), (2, b'\xd8\xa7\xd9\x8a\xd8\xac\xd8\xa7\xd8\xb1')])),
                ('active', models.BooleanField(default=False, verbose_name=b'\xd8\xb9\xd8\xb1\xd8\xb6 \xd9\x81\xd9\x89 \xd8\xa7\xd9\x84\xd8\xb1\xd8\xa6\xd9\x8a\xd8\xb3\xd9\x8a\xd8\xa9')),
                ('entryCity', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.City', null=True)),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='Interface',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xa7\xd8\xac\xd9\x87\xd8\xa9')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Overlooking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Rate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip_address', models.IPAddressField(verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb9\xd9\x86\xd9\x88\xd8\xa7\xd9\x86 ')),
                ('value', models.IntegerField(default=1, verbose_name=b'\xd8\xa7\xd9\x84\xd9\x82\xd9\x8a\xd9\x85\xd9\x87')),
            ],
        ),
        migrations.CreateModel(
            name='Realty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb9\xd9\x86\xd9\x88\xd8\xa7\xd9\x86')),
                ('code', models.IntegerField(unique=True, verbose_name=b'\xd8\xa7\xd9\x84\xd9\x83\xd9\x88\xd8\xaf')),
                ('size', models.DecimalField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x85\xd8\xb3\xd8\xa7\xd8\xad\xd8\xa9', max_digits=10, decimal_places=3)),
                ('garden_size', models.DecimalField(verbose_name=b'\xd9\x85\xd8\xb3\xd8\xa7\xd8\xad\xd8\xa9 \xd8\xa7\xd9\x84\xd8\xad\xd8\xaf\xd9\x8a\xd9\x82\xd8\xa9', max_digits=10, decimal_places=3)),
                ('rooms_count', models.IntegerField(verbose_name=b'\xd8\xb9\xd8\xaf\xd8\xaf \xd8\xa7\xd9\x84\xd8\xba\xd8\xb1\xd9\x81')),
                ('bathrooms_count', models.IntegerField(verbose_name=b'\xd8\xb9\xd8\xaf\xd8\xaf \xd8\xa7\xd9\x84\xd8\xad\xd9\x85\xd8\xa7\xd9\x85\xd8\xa7\xd8\xaa')),
                ('blaconies_count', models.IntegerField(verbose_name=b'\xd8\xb9\xd8\xaf\xd8\xaf \xd8\xa7\xd9\x84\xd8\xa8\xd9\x84\xd9\x83\xd9\x88\xd8\xa7\xd9\x86\xd8\xa7\xd8\xaa')),
                ('price', models.DecimalField(verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb3\xd8\xb9\xd8\xb1', max_digits=15, decimal_places=5)),
                ('visitor_type', models.IntegerField(verbose_name=b'\xd9\x86\xd9\x88\xd8\xb9 \xd8\xa7\xd9\x84\xd8\xb9\xd9\x85\xd9\x8a\xd9\x84', choices=[(1, b'\xd8\xb2\xd8\xa7\xd8\xa6\xd8\xb1'), (2, b'\xd9\x85\xd8\xa7\xd9\x84\xd9\x83')])),
                ('visitor_price', models.DecimalField(verbose_name=b'\xd8\xb3\xd8\xb9\xd8\xb1 \xd8\xa7\xd9\x84\xd8\xb2\xd8\xa7\xd8\xa6\xd8\xb1', max_digits=15, decimal_places=5)),
                ('investors_bulletin', models.BooleanField(default=False, verbose_name=b'\xd9\x86\xd8\xb4\xd8\xb1\xd8\xa9 \xd8\xa7\xd9\x84\xd9\x85\xd8\xb3\xd8\xaa\xd8\xab\xd9\x85\xd8\xb1\xd9\x8a\xd9\x86')),
                ('Finishing', models.BooleanField(default=False, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xaa\xd8\xb4\xd8\xb7\xd9\x8a\xd8\xa8')),
                ('Received', models.BooleanField(default=False, verbose_name=b'\xd9\x85\xd8\xb3\xd8\xaa\xd9\x84\xd9\x85\xd8\xa9')),
                ('description', models.TextField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81')),
                ('purpose', models.IntegerField(verbose_name=b'\xd8\xa7\xd9\x84\xd8\xba\xd8\xb1\xd8\xb6', choices=[(1, b'\xd8\xa8\xd9\x8a\xd8\xb9'), (2, b'\xd8\xa7\xd9\x8a\xd8\xac\xd8\xa7\xd8\xb1')])),
                ('comment', models.TextField(null=True, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xaa\xd8\xb9\xd9\x84\xd9\x8a\xd9\x82', blank=True)),
                ('meta_keywords', models.TextField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd9\x83\xd9\x84\xd9\x85\xd8\xa7\xd8\xaa \xd8\xa7\xd9\x84\xd8\xa8\xd8\xad\xd8\xab\xd9\x8a\xd8\xa9')),
                ('meta_description', models.TextField(verbose_name=b'\xd9\x88\xd8\xb5\xd9\x81 \xd9\x85\xd8\xad\xd8\xb1\xd9\x83\xd8\xa7\xd8\xaa \xd8\xa7\xd9\x84\xd8\xa8\xd8\xad\xd8\xab')),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.City', null=True)),
                ('floor', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.Floor', null=True)),
                ('image', models.OneToOneField(related_name='realty_image', null=True, to='sliders.Photo')),
                ('interface', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.Interface', null=True)),
                ('overlooking', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.Overlooking', null=True)),
                ('pattern', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='patterns.Pattern', null=True)),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='RealtyStage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='RealtyType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='Urgent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb9\xd9\x86\xd9\x88\xd8\xa7\xd9\x86')),
                ('desc', models.TextField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81')),
                ('active', models.BooleanField(default=False, verbose_name=b'\xd8\xb9\xd8\xb1\xd8\xb6 \xd9\x81\xd9\x89 \xd8\xa7\xd9\x84\xd8\xb9\xd8\xa7\xd8\xac\xd9\x84')),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.AddField(
            model_name='realty',
            name='stage',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.RealtyStage', null=True),
        ),
        migrations.AddField(
            model_name='realty',
            name='tags',
            field=models.ManyToManyField(to='others.Tag'),
        ),
        migrations.AddField(
            model_name='realty',
            name='typee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.RealtyType', null=True),
        ),
        migrations.AddField(
            model_name='rate',
            name='realty',
            field=models.ForeignKey(to='realties.Realty'),
        ),
        migrations.AddField(
            model_name='footerentry',
            name='entryTypee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realties.RealtyType', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='rate',
            unique_together=set([('ip_address', 'realty')]),
        ),
    ]
