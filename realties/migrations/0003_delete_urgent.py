# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realties', '0002_remove_realty_image'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Urgent',
        ),
    ]
