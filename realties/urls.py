from django.conf.urls import url
from realties import views

urlpatterns = [
    url(r'show/(?P<pk>\d+)/$', views.show, name='show_realty'),
    url(r'view_city/(?P<pk>\d+)/$', views.view_city),
    url(r'rate/$', 'realties.views.rate', name='rate_realty'),
    url(r'adv_search/', 'realties.views.adv_search', name='adv_search_realty'),
    url(r'search_result/', views.code_search, name='code_search'),
    url(r'list_realties/', views.list_realties, name='list_realties'),
    url(r'view_entry/(?P<pk>\d+)/$', views.view_entry, name='view_entry'),
    url(r'set_compare_params/(?P<param>\d+)/$', views.set_compare_params, name='set_compare_params'),
    url(r'remove_compare_params/(?P<param>\d+)/$', views.remove_compare_params, name='remove_compare_params'),
    url(r'realties_compare/', views.compare_realties, name='compare_realties'),

]
