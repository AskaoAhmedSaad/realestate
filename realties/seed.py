# -*- coding:utf-8 -*-
from realties.models import Realty, City, Floor, Overlooking, RealtyStage, RealtyType
from others.models import Tag
from patterns.models import Pattern
from decimal import *

for items in range(1, 10):
    realty = Realty()
    realty.image = ''
    realty.bathrooms_count = 1
    realty.blaconies_count = 2
    realty.city = City.objects.create(name='مدينتي' + str(items))
    realty.comment = 'any comment'
    realty.floor = Floor.objects.create(name='floor' + str(items))
    realty.size = Decimal('2000.33')
    realty.garden_size = Decimal('200.33')
    realty.overlooking = Overlooking.objects.create(name='overlooking' + str(items))
    realty.typee = RealtyType.objects.create(name='realty type' + str(items))
    realty.status = 1
    realty.rooms_count = 4
    realty.interface = 2
    realty.stage = RealtyStage.objects.create(name='stage' + str(items))
    realty.price = Decimal('2000.33')
    realty.visitor_type = 1
    realty.visitor_price = Decimal('2000.33')
    # realty.tags = Tag.objects.filter(pk=1)[0]
    realty.meta_keywords = 'this is good for searching ' + str(items)
    realty.meta_description = "This site is for geeks"
    realty.save()
