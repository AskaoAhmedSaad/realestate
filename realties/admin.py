from django.contrib import admin
from realties.models import *
from gallery.models import Photo
from django import forms
from django.utils.safestring import mark_safe

class ModelImageWidget(forms.Widget):
    def __init__(self, obj, attrs=None):
        self.object = obj
        super(ModelImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if self.object.pk:
            return mark_safe(
                u'<a href="/media/%s" target="_blank"><img src="/media/%s" width=70 height=70/></a>' 
                % (self.object.path, self.object.path)
            )
        else:
            return mark_safe(u'')


class ImageForm(forms.ModelForm):
    image = forms.CharField(label='Image', required=False)

    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget = ModelImageWidget(self.instance)


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 1
    max_num = 1
    exclude = ('slider', 'title', 'maps', 'pattern', 'patternfloor')


class RealtyAdmin(admin.ModelAdmin):
    inlines = [PhotoInline]
    date_hierarchy = 'timestamp'
    list_per_page = 30
    search_fields = ['comment', 'Received', 'code', 'title']
    list_display = ['__unicode__', 'city', 'typee', 'purpose']
    list_filter = ['investors_bulletin', 'Finishing', 'Received', 'purpose']
    readonly_fields = ['updated', 'timestamp']
    exclude = ['code']



admin.site.register(Realty, RealtyAdmin)
admin.site.register(Rate)
admin.site.register(footerEntry)
admin.site.register(RealtyType)
admin.site.register(City)
admin.site.register(Interface)
admin.site.register(Overlooking)
admin.site.register(RealtyStage)
admin.site.register(Floor)
