# -*- coding:utf-8 -*-
from __future__ import division
from django.db import models
from others.models import Tag
from patterns.models import Pattern
from decimal import *
from django.utils import timezone

VISITOR_TYPE = ((1, 'زائر'), (2, 'مالك'))
PURPOSES = ((1, 'بيع'), (2, 'ايجار'))


class Interface(models.Model):
    name = models.CharField('الواجهة', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class City(models.Model):
    name = models.CharField('الأسم', max_length=255)
    in_statistic_block = models.BooleanField('عرض فى الأحصائيات', default=False)
    in_navbar = models.BooleanField('عرض فى الرئيسية', default=False)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-id']

    def sale_realties_count(self):
        # get all sale_realties in this city
        realties = Realty.objects.filter(city=self, purpose=1)
        return len(realties)

    def rent_realties_count(self):
        # get all rent_realties in this city
        realties = Realty.objects.filter(city=self, purpose=2)
        return len(realties)


class RealtyType(models.Model):
    name = models.CharField('الأسم', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class Overlooking(models.Model):
    name = models.CharField('الأسم', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class Floor(models.Model):
    name = models.CharField('الأسم', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class RealtyStage(models.Model):
    name = models.CharField('الأسم', max_length=255)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-id']


class Rate(models.Model):
    ip_address = models.IPAddressField('العنوان ')
    value = models.IntegerField('القيمه', default=1)
    realty = models.ForeignKey('Realty')

    class Meta:
        unique_together = ('ip_address', 'realty',)

    def __unicode__(self):
        return str(self.ip_address) + ' - ' + self.realty.city.name + ' - ' + self.realty.typee.name + str(self.value)


class Realty(models.Model):
    # image = models.ImageField('الصورة', upload_to='media/upload/maps/', blank=True, null=True)
    # image = models.OneToOneField(Photo ,related_name='realty_image' ,null=True)
    title = models.CharField('العنوان', max_length=255)
    code = models.IntegerField('الكود', unique=True)
    city = models.ForeignKey(City, null=True, on_delete=models.SET_NULL)
    typee = models.ForeignKey(RealtyType, null=True, on_delete=models.SET_NULL)
    size = models.DecimalField('المساحة', max_digits=10, decimal_places=3)
    garden_size = models.DecimalField('مساحة الحديقة', max_digits=10, decimal_places=3)
    rooms_count = models.IntegerField('عدد الغرف')
    bathrooms_count = models.IntegerField('عدد الحمامات')
    blaconies_count = models.IntegerField('عدد البلكوانات')
    pattern = models.ForeignKey(Pattern, blank=True, null=True, on_delete=models.SET_NULL)
    overlooking = models.ForeignKey(Overlooking, null=True, on_delete=models.SET_NULL)
    floor = models.ForeignKey(Floor, null=True, on_delete=models.SET_NULL)
    interface = models.ForeignKey(Interface, null=True, on_delete=models.SET_NULL)
    stage = models.ForeignKey(RealtyStage, null=True, on_delete=models.SET_NULL)
    price = models.DecimalField('السعر', max_digits=15, blank=False, decimal_places=5)
    visitor_type = models.IntegerField('نوع العميل', choices=VISITOR_TYPE)
    visitor_price = models.DecimalField('سعر الزائر', max_digits=15, blank=False, decimal_places=5)
    investors_bulletin = models.BooleanField('نشرة المستثمرين', default=False)
    Finishing = models.BooleanField('التشطيب', default=False)
    Received = models.BooleanField('مستلمة', default=False)
    tags = models.ManyToManyField(Tag)
    description = models.TextField('الوصف')
    purpose = models.IntegerField('الغرض', choices=PURPOSES)
    comment = models.TextField('التعليق', blank=True, null=True)
    meta_keywords = models.TextField('الكلمات البحثية', max_length=255)
    meta_description = models.TextField('وصف محركات البحث')
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-id']

    # getting the average of the rates of this realty
    def avearage_rate(self):
        rates_of_this_realty = self.rate_set.all()
        counts = rates_of_this_realty.count()
        sums = 0
        for rates in rates_of_this_realty:
            sums += rates.value
        if counts == 0:
            return 0
        else:
            return sums / (5 * counts)

    # when the firs time to create realty it take code by default during saving

    def save(self, *args, **kwargs):
        if not self.id:
            realty = Realty.objects.first()
            if not realty:  # if the object is not the first one make it's code 1000
                self.code = 1000
            else:  # if the object is the first one add 1 to it's number
                self.code = realty.code +1
        super(Realty, self).save(*args, **kwargs)


class footerEntry(models.Model):
    entryTypee = models.ForeignKey(RealtyType, null=True, on_delete=models.SET_NULL)
    purpose = models.IntegerField('الغرض', choices=PURPOSES)
    entryCity = models.ForeignKey(City, null=True, on_delete=models.SET_NULL)
    active = models.BooleanField('عرض فى الرئيسية', default=False)

    def __unicode__(self):
        if self.purpose == 1:
            self.purpose = u'بيع'
        elif self.purpose == 2:
            self.purpose = u'ايجار'
        return "%s %s %s" % ( self.entryTypee.name, self.purpose, self.entryCity.name)

    class Meta:
        ordering = ['-id']

