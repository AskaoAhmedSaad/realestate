# -*- coding:utf-8 -*-
from realties.models import Realty, City, Floor, Overlooking, RealtyStage, RealtyType, Interface, footerEntry
from others.models import Tag
from patterns.models import Pattern, PatternType, PatternTypeStage, PatternFloor
from maps.models import *
from gallery.models import *
from sliders.models import Slider

# deleting photos only one time

Photo.objects.all().delete()

# //////////////////////////////////////

# inserting maps Type

MapType.objects.all().delete()

mapType1 = MapType(name='خرائط العمارات بالرحاب', description='')
mapType1.save()
mapType2 = MapType(name='خرائط الفيلات بالرحاب', description='')
mapType2.save()
mapType3 = MapType(name='خرائط العمارات بمدينتي', description='')
mapType3.save()
mapType4 = MapType(name='خرائط الفيلات بمدينتي', description='')
mapType4.save()


# //////////////////////////////////////

# inserting mapsType stages

MapTypeStage.objects.all().delete()

mapTypeStage1 = MapTypeStage(name='المرحلة الأولي', typee=mapType1, description='')
mapTypeStage1.save()
mapTypeStage2 = MapTypeStage(name='المرحلة الثانية', typee=mapType1 , description='')
mapTypeStage2.save()
mapTypeStage3 = MapTypeStage(name='المرحلة الثالثة', typee=mapType1, description='')
mapTypeStage3.save()
mapTypeStage4 = MapTypeStage(name='المرحلة الرابعة', typee=mapType1, description='')
mapTypeStage4.save()

mapTypeStage5 = MapTypeStage(name='المرحلة الأولي', typee=mapType2, description='')
mapTypeStage5.save()
mapTypeStage6 = MapTypeStage(name='المرحلة الثانية', typee=mapType2, description='')
mapTypeStage6.save()
mapTypeStage7 = MapTypeStage(name='المرحلة الثالثة', typee=mapType2, description='')
mapTypeStage7.save()
mapTypeStage8 = MapTypeStage(name='المرحلة الرابعة', typee=mapType2, description='')
mapTypeStage8.save()

mapTypeStage9 = MapTypeStage(name='المرحلة الأولي', typee=mapType3, description='')
mapTypeStage9.save()
mapTypeStage10 = MapTypeStage(name='المرحلة الثانية', typee=mapType3, description='')
mapTypeStage10.save()
mapTypeStage11 = MapTypeStage(name='المرحلة الثالثة', typee=mapType3, description='')
mapTypeStage11.save()
mapTypeStage12 = MapTypeStage(name='المرحلة الرابعة', typee=mapType3, description='')
mapTypeStage12.save()

mapTypeStage13 = MapTypeStage(name='المرحلة الأولي', typee=mapType4, description='')
mapTypeStage13.save()
mapTypeStage14 = MapTypeStage(name='المرحلة الثانية', typee=mapType4, description='')
mapTypeStage14.save()
mapTypeStage15 = MapTypeStage(name='المرحلة الثالثة', typee=mapType4, description='')
mapTypeStage15.save()
mapTypeStage16 = MapTypeStage(name='المرحلة الرابعة', typee=mapType4, description='')
mapTypeStage16.save()

# //////////////////////////////////////

# inserting maps

Map.objects.all().delete()

map1 = Map(name='', typee=mapType1, stage=mapTypeStage1)
map1.save()
mapImage1 = Photo(title=map1.name, image='media/upload/map1.jpg', maps=map1)
mapImage1.save()

map2 = Map(name='', typee=mapType1, stage=mapTypeStage2)
map2.save()
mapImage2 = Photo(title=map2.name, image='media/upload/map2.jpg', maps=map2)
mapImage2.save()

map3 = Map(name='', typee=mapType1, stage=mapTypeStage3)
map3.save()
mapImage3 = Photo(title=map3.name, image='media/upload/map3.jpg', maps=map3)
mapImage3.save()

map4 = Map(name='', typee=mapType1, stage=mapTypeStage4)
map4.save()
mapImage4 = Photo(title=map4.name, image='media/upload/map4.jpg', maps=map4)
mapImage4.save()


map5 = Map(name='', typee=mapType2, stage=mapTypeStage1)
map5.save()
mapImage5 = Photo(title=map5.name, image='media/upload/map5.jpg', maps=map5)
mapImage5.save()

map6 = Map(name='', typee=mapType2, stage=mapTypeStage2)
map6.save()
mapImage6 = Photo(title=map6.name, image='media/upload/map6.jpg', maps=map6)
mapImage6.save()

map7 = Map(name='', typee=mapType2, stage=mapTypeStage3)
map7.save()
mapImage7 = Photo(title=map7.name, image='media/upload/map7.jpg', maps=map7)
mapImage7.save()

map8 = Map(name='', typee=mapType2, stage=mapTypeStage4)
map8.save()
mapImage8 = Photo(title=map8.name, image='media/upload/map8.jpg', maps=map8)
mapImage8.save()

map9 = Map(name='', typee=mapType3, stage=mapTypeStage1)
map9.save()
mapImage9 = Photo(title=map9.name, image='media/upload/map9.jpg', maps=map9)
mapImage9.save()

map10 = Map(name='', typee=mapType3, stage=mapTypeStage2)
map10.save()
mapImage10 = Photo(title=map10.name, image='media/upload/map10.jpg', maps=map10)
mapImage10.save()

map11 = Map(name='', typee=mapType3, stage=mapTypeStage3)
map11.save()
mapImage11 = Photo(title=map11.name, image='media/upload/map11.jpg', maps=map11)
mapImage11.save()

map12 = Map(name='', typee=mapType3, stage=mapTypeStage4)
map12.save()
mapImage12 = Photo(title=map12.name, image='media/upload/map12.jpg', maps=map12)
mapImage12.save()

map13 = Map(name='', typee=mapType4, stage=mapTypeStage1)
map13.save()
mapImage13 = Photo(title=map13.name, image='media/upload/map13.jpg', maps=map13)
mapImage13.save()

map14 = Map(name='', typee=mapType4, stage=mapTypeStage2)
map14.save()
mapImage14 = Photo(title=map14.name, image='media/upload/map14.jpg', maps=map14)
mapImage14.save()

map15 = Map(name='', typee=mapType4, stage=mapTypeStage3)
map15.save()
mapImage15 = Photo(title=map15.name, image='media/upload/map15.jpg', maps=map15)
mapImage15.save()

map16 = Map(name='', typee=mapType4, stage=mapTypeStage4)
map16.save()
mapImage16 = Photo(title=map16.name, image='media/upload/map16.jpg', maps=map16)
mapImage16.save()
# //////////////////////////////////////

# inser slider
Slider.objects.all().delete()


slider1 = Slider(title="شرم الشيخ محل 25 م2 للبيع بالتقسيط استلام فوري", active=True, 
        description='بشرم الشيخ محل بمساحة 25 م2 تشطيب الشركة استلام فوري بمقدم 0 ج ')
slider1.save()

sliderImage1 = Photo(title=slider1.title, image='media/upload/slider1.jpeg', slider=slider1)
sliderImage1.save()


slider2 = Slider(title="شرم الشيخ شقة 45 م2 للبيع كاش تشطيب سوبر لوكس استلام فوري 18,000 ج ", active=True, 
        description='بشرم الشيخ شقة بمساحة 45 م2 بالطابق الأول المرحلة غير متاح تطل على غير متاح عبارة عن نوم حمام تشطيب سوبر لوكس للبيع كاش استلام فوري 18,000 ج للبيع كاش ')
slider2.save()


sliderImage2 = Photo(title=slider2.title, image='media/upload/slider2.jpeg', slider=slider2)
sliderImage2.save()



slider3 = Slider(title="العين السخنة شقة الشاليه أرضى مساحته 70 متر2 وحديقه 30 متر2 م2 بحري للبيع كاش على حمام السباحة استلام فوري 290,000 ج",
        active=True, 
        description='بالعين السخنة شقة بمساحة الشاليه أرضى مساحته 70 متر2 وحديقه 30 متر2 م2 ب المرحلة غير متاح النموذج ( 3 ) بحري تطل على حمام السباحة عبارة عن 2 نوم 1 حمام تشطيب لوكس للبيع كاش استلام فوري 290,000 ج للبيع كاش')
slider3.save()

sliderImage3 = Photo(title=slider3.title, image='media/upload/slider3.jpeg', slider=slider3)
sliderImage3.save()


slider4 = Slider(title="العين السخنة شقة 90 م2 بحري للبيع كاش على حمام السباحة تشطيب سوبر لوكس استلام فوري 73,500 ج",
        active=True, 
        description='بالعين السخنة شقة بمساحة 90 م2 بالطابق الأول المرحلة غير متاح النموذج ( 1 ) بحري تطل على حمام السباحة عبارة عن 2 نوم 1 حمام تشطيب سوبر لوكس للبيع كاش استلام فوري 73,500 ج للبيع كاش')
slider4.save()

sliderImage4 = Photo(title=slider4.title, image='media/upload/slider4.jpeg', slider=slider4)
sliderImage4.save()


slider5 = Slider(title="العريش فيلا ٤٢٠ م2 بحري للبيع كاش تشطيب سوبر لوكس استلام فوري 420 ج ",
        active=True, 
        description='بالعريش فيلا ثنائية بمساحة ٤٢٠ م2 ومساحة مباني ١٥٠ م2 المرحلة غير متاح النموذج ( 0 ) بحري تطل على شارع عبارة عن ٦ نوم ٣ حمام تشطيب سوبر لوكس للبيع كاش استلام فوري 420 ج للبيع كاش ')
slider5.save()

sliderImage4 = Photo(title=slider5.title, image='media/upload/slider5.jpeg', slider=slider5)
sliderImage4.save()


# //////////////////////////////////////

PatternType.objects.all().delete()

pattern_type1 = PatternType(name="نماذج العمارات بالرحاب", description="نماذج العمارات بالرحاب")
pattern_type1.save()
pattern_type2 = PatternType(name="نماذج العمارات بمدينتي", description="نماذج العمارات بمدينتي")
pattern_type2.save()
pattern_type3 = PatternType(name="نماذج الفيلات بالرحاب", description="نماذج الفيلات بالرحاب")
pattern_type3.save()
pattern_type4 = PatternType(name="نماذج الفيلات بمدينتي", description="نماذج الفيلات بمدينتي")
pattern_type4.save()

# //////////////////////////////////////

PatternTypeStage.objects.all().delete()

Pattern_typeStage1 = PatternTypeStage(name="النموذج 100", description="النموذج 100", typee=pattern_type1)
Pattern_typeStage1.save()
Pattern_typeStage2 = PatternTypeStage(name="النموذج 200", description="النموذج 200", typee=pattern_type1)
Pattern_typeStage2.save()
Pattern_typeStage3 = PatternTypeStage(name="النموذج 300", description="النموذج 300", typee=pattern_type1)
Pattern_typeStage3.save()
Pattern_typeStage4 = PatternTypeStage(name="النموذج 400", description="النموذج 400", typee=pattern_type1)
Pattern_typeStage4.save()

Pattern_typeStage5 = PatternTypeStage(name="النموذج 500", description="النموذج 500", typee=pattern_type2)
Pattern_typeStage5.save()
Pattern_typeStage6 = PatternTypeStage(name="النموذج 600", description="النموذج 600", typee=pattern_type2)
Pattern_typeStage6.save()
Pattern_typeStage7 = PatternTypeStage(name="النموذج 700", description="النموذج 700", typee=pattern_type2)
Pattern_typeStage7.save()
Pattern_typeStage8 = PatternTypeStage(name="النموذج 800", description="النموذج 800", typee=pattern_type2)
Pattern_typeStage8.save()

Pattern_typeStage9 = PatternTypeStage(name="النموذج 900", description="النموذج 900", typee=pattern_type3)
Pattern_typeStage9.save()
Pattern_typeStage10 = PatternTypeStage(name="النموذج 1000", description="النموذج 1000", typee=pattern_type3)
Pattern_typeStage10.save()
Pattern_typeStage11 = PatternTypeStage(name="النموذج 1100", description="النموذج 1100", typee=pattern_type3)
Pattern_typeStage11.save()
Pattern_typeStage12 = PatternTypeStage(name="النموذج 1200", description="النموذج 1200", typee=pattern_type3)
Pattern_typeStage12.save()

Pattern_typeStage13 = PatternTypeStage(name="النموذج 1300", description="النموذج 1300", typee=pattern_type4)
Pattern_typeStage13.save()
Pattern_typeStage14 = PatternTypeStage(name="النموذج 1400", description="النموذج 1400", typee=pattern_type4)
Pattern_typeStage14.save()
Pattern_typeStage15 = PatternTypeStage(name="النموذج 1500", description="النموذج 1500", typee=pattern_type4)
Pattern_typeStage15.save()
Pattern_typeStage16 = PatternTypeStage(name="النموذج 1600", description="النموذج 1600", typee=pattern_type4)
Pattern_typeStage16.save()

# //////////////////////////////////////

Pattern.objects.all().delete()

pattern1 = Pattern(name="الأولي", typee=pattern_type1, stage=Pattern_typeStage1)
pattern1.save()

patternImage1 = Photo(title=pattern1.name, image='media/upload/pattern1.jpg', pattern=pattern1)
patternImage1.save()


pattern2 = Pattern(name="الثانية", typee=pattern_type1, stage=Pattern_typeStage1)
pattern2.save()

patternImage2 = Photo(title=pattern2.name, image='media/upload/pattern2.jpg', pattern=pattern2)
patternImage2.save()


pattern3 = Pattern(name="الثالثة", typee=pattern_type1, stage=Pattern_typeStage2)
pattern3.save()

patternImage3 = Photo(title=pattern3.name, image='media/upload/pattern3.jpg', pattern=pattern3)
patternImage3.save()


pattern4 = Pattern(name="الرابعة", typee=pattern_type1, stage=Pattern_typeStage2)
pattern4.save()

patternImage4 = Photo(title=pattern4.name, image='media/upload/pattern4.jpg', pattern=pattern4)
patternImage4.save()


pattern5 = Pattern(name="الخامسة", typee=pattern_type1, stage=Pattern_typeStage3)
pattern5.save()

patternImage5 = Photo(title=pattern5.name, image='media/upload/pattern5.jpg', pattern=pattern5)
patternImage5.save()


pattern6 = Pattern(name="السادسة", typee=pattern_type1, stage=Pattern_typeStage3)
pattern6.save()

patternImage6 = Photo(title=pattern6.name, image='media/upload/pattern6.jpg', pattern=pattern6)
patternImage6.save()


pattern7 = Pattern(name="السابعة", typee=pattern_type1, stage=Pattern_typeStage4)
pattern7.save()

patternImage7 = Photo(title=pattern7.name, image='media/upload/pattern7.jpg', pattern=pattern7)
patternImage7.save()


pattern8 = Pattern(name="الثامنة", typee=pattern_type1, stage=Pattern_typeStage4)
pattern8.save()

patternImage8 = Photo(title=pattern8.name, image='media/upload/pattern8.jpg', pattern=pattern8)
patternImage8.save()


pattern9 = Pattern(name="التاسعة", typee=pattern_type2, stage=Pattern_typeStage5)
pattern9.save()

patternImage9 = Photo(title=pattern9.name, image='media/upload/pattern9.jpg', pattern=pattern9)
patternImage9.save()


pattern10 = Pattern(name="العاشرة", typee=pattern_type2, stage=Pattern_typeStage5)
pattern10.save()

patternImage10 = Photo(title=pattern10.name, image='media/upload/pattern10.jpg', pattern=pattern10)
patternImage10.save()


pattern11 = Pattern(name="الحادي عشر", typee=pattern_type2, stage=Pattern_typeStage6)
pattern11.save()

patternImage11 = Photo(title=pattern11.name, image='media/upload/pattern11.jpg', pattern=pattern11)
patternImage11.save()


pattern12 = Pattern(name="الثاني عشر", typee=pattern_type2, stage=Pattern_typeStage6)
pattern12.save()

patternImage12 = Photo(title=pattern12.name, image='media/upload/pattern12.jpg', pattern=pattern12)
patternImage12.save()


pattern13 = Pattern(name="الثالث عشر", typee=pattern_type2, stage=Pattern_typeStage7)
pattern13.save()

patternImage13 = Photo(title=pattern13.name, image='media/upload/pattern13.jpg', pattern=pattern13)
patternImage13.save()


pattern14 = Pattern(name="الرابع عشر", typee=pattern_type2, stage=Pattern_typeStage7)
pattern14.save()

patternImage14 = Photo(title=pattern14.name, image='media/upload/pattern14.jpg', pattern=pattern14)
patternImage14.save()


pattern15 = Pattern(name="الخامس عشر", typee=pattern_type2, stage=Pattern_typeStage8)
pattern15.save()

patternImage15 = Photo(title=pattern15.name, image='media/upload/pattern15.jpg', pattern=pattern15)
patternImage15.save()


pattern16 = Pattern(name="السادس عشر", typee=pattern_type2, stage=Pattern_typeStage8)
pattern16.save()

patternImage16 = Photo(title=pattern16.name, image='media/upload/pattern16.jpg', pattern=pattern16)
patternImage16.save()


pattern17 = Pattern(name="السابع عشر", typee=pattern_type3, stage=Pattern_typeStage9)
pattern17.save()

patternImage17 = Photo(title=pattern17.name, image='media/upload/pattern17.jpg', pattern=pattern17)
patternImage17.save()


pattern18 = Pattern(name="الثامن عشر", typee=pattern_type3, stage=Pattern_typeStage9)
pattern18.save()
pattern19 = Pattern(name="التاسع عشر", typee=pattern_type3, stage=Pattern_typeStage10)
pattern19.save()
pattern20 = Pattern(name="العشرون", typee=pattern_type4, stage=Pattern_typeStage13)
pattern20.save()
pattern21 = Pattern(name="الحادي والعشرون", typee=pattern_type4, stage=Pattern_typeStage13)
pattern21.save()
pattern22 = Pattern(name="الثاني والعشرون", typee=pattern_type4, stage=Pattern_typeStage14)
pattern22.save()
pattern23 = Pattern(name="الثالث والعشرون", typee=pattern_type4, stage=Pattern_typeStage15)
pattern23.save()
pattern24 = Pattern(name="الرابع والعشرون", typee=pattern_type4, stage=Pattern_typeStage15)
pattern24.save()
pattern25 = Pattern(name="الخامس والعشرون", typee=pattern_type4, stage=Pattern_typeStage16)
pattern25.save()
pattern26 = Pattern(name="السادس والعشرون", typee=pattern_type4, stage=Pattern_typeStage16)
pattern26.save()


# //////////////////////////////////////

# insert PatternFloors

PatternFloor.objects.all().delete()

patternFloor1 = PatternFloor(name='النموذج S', patten=pattern1)
patternFloor1.save()

patternFloorImage1 = Photo(title=patternFloor1.name, image='media/upload/patternFloor1.jpg', patternfloor=patternFloor1)
patternFloorImage1.save()


patternFloor2 = PatternFloor(name='النموذج X', patten=pattern2)
patternFloor2.save()

patternFloorImage2 = Photo(title=patternFloor2.name, image='media/upload/patternFloor2.jpg', patternfloor=patternFloor2)
patternFloorImage2.save()


patternFloor3 = PatternFloor(name='النموذج W', patten=pattern3)
patternFloor3.save()

patternFloorImage3 = Photo(title=patternFloor3.name, image='media/upload/patternFloor3.jpg', patternfloor=patternFloor3)
patternFloorImage3.save()


patternFloor4 = PatternFloor(name='النموذج Y', patten=pattern4)
patternFloor4.save()

patternFloorImage4 = Photo(title=patternFloor4.name, image='media/upload/patternFloor4.jpg', patternfloor=patternFloor4)
patternFloorImage4.save()


patternFloor5 = PatternFloor(name='النموذج Z', patten=pattern5)
patternFloor5.save()

patternFloorImage5 = Photo(title=patternFloor5.name, image='media/upload/patternFloor5.jpg', patternfloor=patternFloor5)
patternFloorImage5.save()


patternFloor6 = PatternFloor(name='النموذج R', patten=pattern6)
patternFloor6.save()

patternFloorImage6 = Photo(title=patternFloor6.name, image='media/upload/patternFloor6.jpg', patternfloor=patternFloor6)
patternFloorImage6.save()


patternFloor7 = PatternFloor(name='النموذج B', patten=pattern7)
patternFloor7.save()

patternFloorImage7 = Photo(title=patternFloor7.name, image='media/upload/patternFloor7.jpg', patternfloor=patternFloor7)
patternFloorImage7.save()


patternFloor8 = PatternFloor(name='النموذج S', patten=pattern1)
patternFloor8.save()

patternFloorImage8 = Photo(title=patternFloor8.name, image='media/upload/patternFloor8.jpg', patternfloor=patternFloor8)
patternFloorImage8.save()

patternFloor9 = PatternFloor(name='النموذج X', patten=pattern2)
patternFloor9.save()

patternFloorImage9 = Photo(title=patternFloor9.name, image='media/upload/patternFloor9.jpg', patternfloor=patternFloor9)
patternFloorImage9.save()

patternFloor10 = PatternFloor(name='النموذج W', patten=pattern3)
patternFloor10.save()

patternFloorImage10 = Photo(title=patternFloor10.name, image='media/upload/patternFloor10.jpg', patternfloor=patternFloor10)
patternFloorImage10.save()

patternFloor11 = PatternFloor(name='النموذج Y', patten=pattern4)
patternFloor11.save()

patternFloorImage11 = Photo(title=patternFloor11.name, image='media/upload/patternFloor11.jpg', patternfloor=patternFloor11)
patternFloorImage11.save()

patternFloor12 = PatternFloor(name='النموذج Z', patten=pattern5)
patternFloor12.save()

patternFloorImage12 = Photo(title=patternFloor12.name, image='media/upload/patternFloor12.jpg', patternfloor=patternFloor12)
patternFloorImage12.save()

patternFloor13 = PatternFloor(name='النموذج R', patten=pattern6)
patternFloor13.save()

patternFloorImage13 = Photo(title=patternFloor13.name, image='media/upload/patternFloor13.jpg', patternfloor=patternFloor13)
patternFloorImage13.save()

patternFloor14 = PatternFloor(name='النموذج B', patten=pattern7)
patternFloor14.save()

patternFloorImage14 = Photo(title=patternFloor14.name, image='media/upload/patternFloor14.jpg', patternfloor=patternFloor14)
patternFloorImage14.save()




# //////////////////////////////////////


City.objects.all().delete()

city1 = City(name="الرحاب", in_navbar=True, in_statistic_block=True)
city1.save()
city2 = City(name="الشروق", in_navbar=True, in_statistic_block=True)
city2.save()
city3 = City(name="السادس من اكتوبر", in_navbar=True, in_statistic_block=True)
city3.save()
city4 = City(name="العامرية", in_navbar=True)
city4.save()
city5 = City(name="العاشر من رمضان", in_navbar=True, in_statistic_block=True)
city5.save()
city6 = City(name="المحلة", in_navbar=True)
city6.save()
city7 = City(name="المنصورة", in_navbar=True, in_statistic_block=True)
city7.save()
city8 = City(name="القادسية", in_navbar=True)
city8.save()
city9 = City(name="العريش", in_navbar=True, in_statistic_block=True)
city9.save()
city10 = City(name="مدينتي", in_navbar=True, in_statistic_block=True)
city10.save()

# //////////////////////////////////////

Overlooking.objects.all().delete()

over_looking1 = Overlooking(name="علي حديقه")
over_looking1.save()
over_looking2 = Overlooking(name="علي بحر")
over_looking2.save()
over_looking3 = Overlooking(name="علي شارع رئيسي")
over_looking3.save()
over_looking4 = Overlooking(name="علي شارع جانبي")
over_looking4.save()


# //////////////////////////////////////


RealtyType.objects.all().delete()

realty_Type1 = RealtyType(name="فيلا")
realty_Type1.save()
realty_Type2 = RealtyType(name="شالية")
realty_Type2.save()
realty_Type3 = RealtyType(name="شقة")
realty_Type3.save()
realty_Type4 = RealtyType(name="المحل")
realty_Type4.save()

# //////////////////////////////////////

Floor.objects.all().delete()

floor1 = Floor(name="طابق الأول")
floor1.save()
floor2 = Floor(name="طابق الثاني")
floor2.save()
floor3 = Floor(name="طابق الثالث")
floor3.save()
floor4 = Floor(name="طابق الرابع")
floor4.save()
floor5 = Floor(name="طابق الخامس")
floor5.save()
floor6 = Floor(name="طابق السادس")
floor6.save()
floor7 = Floor(name="طابق السابع")
floor7.save()
floor8 = Floor(name="طابق الثامن")
floor8.save()

# //////////////////////////////////////


Interface.objects.all().delete()

interface1 = Interface(name="بجري")
interface1.save()
interface2 = Interface(name="قبلي")
interface2.save()

# //////////////////////////////////////

RealtyStage.objects.all().delete()

realty_stage1 = RealtyStage(name="مرحلة الأولي")
realty_stage1.save()
realty_stage2 = RealtyStage(name="مرحلة الثانية")
realty_stage2.save()
realty_stage3 = RealtyStage(name="مرحلة الثالثة")
realty_stage3.save()
realty_stage4 = RealtyStage(name="مرحلة الرابعة")
realty_stage4.save()
realty_stage5 = RealtyStage(name="مرحلة الخامسة")
realty_stage5.save()
realty_stage6 = RealtyStage(name="مرحلة السادسة")
realty_stage6.save()
realty_stage7 = RealtyStage(name="مرحلة السابعة")
realty_stage7.save()
realty_stage8 = RealtyStage(name="مرحلة الثامنة")
realty_stage8.save()

# //////////////////////////////////////

Tag.objects.all().delete()

tag1 = Tag(name="tag1")
tag1.save()
tag2 = Tag(name="tag2")
tag2.save()
tag3 = Tag(name="tag3")
tag3.save()
tag4 = Tag(name="tag4")
tag4.save()
tag5 = Tag(name="tag5")
tag5.save()
tag6 = Tag(name="tag6")
tag6.save()
tag7 = Tag(name="tag7")
tag7.save()

# //////////////////////////////////////

# insert realties and its photo

Realty.objects.all().delete()


realty1 = Realty(city=city1, title=u"مدينة نصر شقة 205 م2 للبيع كاش استلام فوري 875,000 ج ", typee=realty_Type1, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=1,
                blaconies_count=1, pattern=pattern1, overlooking=over_looking1, floor=floor1, interface=interface1,
                stage=realty_stage1,
                price=256000, visitor_type=1, visitor_price=275000, Finishing=True, Received=True,
                investors_bulletin=False, meta_description="realty1",
                purpose=1, comment="", meta_keywords='realty1',
                description=u'بمدينة نصر شقة بمساحة 205 م2 بالطابق الخامس المرحلة غير متاح تطل على شارع عبارة عن 3 نوم 2 حمام غير متاح للبيع كاش استلام فوري 875,000 ج للبيع كاش')
realty1.save()
realty1.tags.add(tag1, tag2)
realty1.save()

realtyImage1 = Photo(title=realty1.title, image='media/upload/realty1.jpeg', realty=realty1)
realtyImage1.save()


realty2 = Realty(city=city2, title=u"مدينة نصر محل 40 م2 بحري للإيجار قانون ج تشطيب خاص 11,000 ج", typee=realty_Type1, size=130, garden_size=40, rooms_count=2,
                bathrooms_count=1,
                blaconies_count=3, pattern=pattern1, overlooking=over_looking2, floor=floor2, interface=interface2,
                stage=realty_stage2,
                price=300000, visitor_type=2, visitor_price=300000, Finishing=False, Received=True,
                investors_bulletin=False, meta_description="realty2",
                purpose=2, comment="", meta_keywords='realty2',description=u'بمدينة نصر محل بمساحة 40 م2 تشطيب خاص للإيجار قانون ج 11,000 ج ')
realty2.save()
realty2.tags.add(tag1, tag2)
realty2.save()

realtyImage2 = Photo(title=realty2.title, image='media/upload/realty2.jpg', realty=realty2)
realtyImage2.save()


realty3 = Realty(city=city3, title=u"مدينة نصر شقة 200 م2 بحري للبيع كاش على حديقة كبيرة استلام فوري 870,000 ج", typee=realty_Type2, size=120, garden_size=40, rooms_count=3,
                bathrooms_count=3,
                blaconies_count=3, pattern=pattern2, overlooking=over_looking3, floor=floor3, interface=interface1,
                stage=realty_stage1,
                price=356000, visitor_type=1, visitor_price=375000, Finishing=True, Received=False,
                investors_bulletin=True, meta_description="realty3",
                purpose=1, comment="", meta_keywords='realty3',
                description=u'بمدينة نصر شقة بمساحة 200 م2 بالطابق السابع المرحلة غير متاح بحري تطل على حديقة كبيرة عبارة عن 3 نوم 2 حمام تشطيب هاي لوكس للبيع كاش استلام فوري 870,000 ج للبيع كاش ')
realty3.save()
realty3.tags.add(tag3)
realty3.save()


realtyImage3 = Photo(title=realty3.title, image='media/upload/realty3.jpeg', realty=realty3)
realtyImage3.save()


realty4 = Realty(city=city4, title=u"مدينة نصر شقة 225 م2 بحري للبيع كاش استلام فوري 1,080,000 ج ", typee=realty_Type2, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern2, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=True, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 225 م2 بالطابق الثالث المرحلة غير متاح النموذج ( 9 ) بحري و قبلي تطل على شارع عبارة عن 3 نوم 3 حمام تشطيب هاي لوكس للبيع كاش استلام فوري 1,080,000 ج للبيع كاش ')
realty4.save()
realty4.tags.add(tag4)
realty4.save()

realtyImage4 = Photo(title=realty4.title, image='media/upload/realty4.jpeg', realty=realty4)
realtyImage4.save()

realty5 = Realty(city=city4, title=u"مدينة نصر شقة 255 م2 بحري للبيع كاش على حديقة كبيرة استلام فوري 1,250,000 ج ", typee=realty_Type3, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern3, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 255 م2 ب المرحلة غير متاح بحري و غربي تطل على حديقة كبيرة عبارة عن 4 نوم 3 حمام تشطيب لوكس للبيع كاش استلام فوري 1,250,000 ج للبيع كاش')
realty5.save()
realty5.tags.add(tag4)
realty5.save()

realtyImage5 = Photo(title=realty5.title, image='media/upload/realty5.jpeg', realty=realty5)
realtyImage5.save()


realty6 = Realty(city=city4, title=u"مدينة نصر شقة 200 م2 بحري للبيع كاش على حديقة كبيرة استلام فوري 900,000 ج ", typee=realty_Type3, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern3, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=True, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 200 م2 بالطابق السابع المرحلة غير متاح بحري تطل على حديقة كبيرة عبارة عن 3 نوم 2 حمام تشطيب هاي لوكس للبيع كاش استلام فوري 900,000 ج للبيع كاش ')
realty6.save()
realty6.tags.add(tag4)
realty6.save()


realtyImage6 = Photo(title=realty6.title, image='media/upload/realty6.jpeg', realty=realty6)
realtyImage6.save()


realty7 = Realty(city=city4, title=u"مدينة نصر شقة 300 م2 للبيع كاش على حديقة كبيرة استلام فوري 1,800,000 ج ", typee=realty_Type4, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern4, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 300 م2 ب المرحلة غير متاح النموذج ( 12 ) قبلي تطل على حديقة كبيرة عبارة عن 3 نوم 4 حمام تشطيب هاي لوكس للبيع كاش استلام فوري 1,800,000 ج للبيع كاش ')
realty7.save()
realty7.tags.add(tag4)
realty7.save()


realtyImage7 = Photo(title=realty7.title, image='media/upload/realty7.jpeg', realty=realty7)
realtyImage7.save()


realty8 = Realty(city=city4, title=u"مدينة نصر شقة 200 م2 بحري للبيع كاش على حديقة كبيرة 920,000 ج ", typee=realty_Type4, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern4, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=True, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 200 م2 بالطابق السابع المرحلة غير متاح بحري تطل على حديقة كبيرة عبارة عن 3 نوم 2 حمام تشطيب هاي لوكس للبيع كاش 920,000 ج للبيع كاش')
realty8.save()
realty8.tags.add(tag4)
realty8.save()


realtyImage8 = Photo(title=realty8.title, image='media/upload/realty8.jpg', realty=realty8)
realtyImage8.save()


realty9 = Realty(city=city4, title=u"مدينة نصر محل 340 م2 للبيع كاش استلام فوري 5,000,000 ج", typee=realty_Type1, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern5, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=True, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر محل بمساحة 340 م2 غير متاح للبيع كاش استلام فوري 5,000,000 ج للبيع كاش ')
realty9.save()
realty9.tags.add(tag4)
realty9.save()


realtyImage9 = Photo(title=realty9.title, image='media/upload/realty9.jpeg', realty=realty9)
realtyImage9.save()

realty10 = Realty(city=city4, title=u"مدينة نصر شقة 86 م2 للبيع كاش تشطيب سوبر لوكس استلام فوري 260,000 ج", typee=realty_Type2, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern5, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 86 م2 ب المرحلة غير متاح تطل على غير متاح عبارة عن نوم حمام تشطيب سوبر لوكس للبيع كاش استلام فوري 260,000 ج للبيع كاش ')
realty10.save()
realty10.tags.add(tag4)
realty10.save()

realtyImage10 = Photo(title=realty10.title, image='media/upload/realty10.jpeg', realty=realty10)
realtyImage10.save()


realty11 = Realty(city=city4, title=u"مدينة نصر شقة 200 م2 بحري للبيع كاش على حديقة كبيرة 920,000 ج ", typee=realty_Type3, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern6, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 200 م2 بالطابق السابع المرحلة غير متاح بحري تطل على حديقة كبيرة عبارة عن 3 نوم 2 حمام تشطيب هاي لوكس للبيع كاش 920,000 ج للبيع كاش ')
realty11.save()
realty11.tags.add(tag4)
realty11.save()

realtyImage11 = Photo(title=realty11.title, image='media/upload/realty11.jpg', realty=realty11)
realtyImage11.save()


realty12 = Realty(city=city4, title=u"مدينة نصر شقة 200 م2 بحري للبيع كاش على حديقة كبيرة 920,000 ج ", typee=realty_Type4, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern6, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=True, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر شقة بمساحة 200 م2 بالطابق السابع المرحلة غير متاح بحري تطل على حديقة كبيرة عبارة عن 3 نوم 2 حمام تشطيب هاي لوكس للبيع كاش 920,000 ج للبيع كاش')
realty12.save()
realty12.tags.add(tag4)
realty12.save()

realtyImage12 = Photo(title=realty12.title, image='media/upload/realty12.jpeg', realty=realty12)
realtyImage12.save()


realty13 = Realty(city=city4, title=u"مدينة نصر فيلا 500 م2 بحري للإيجار قانون ج 12,000 ج ", typee=realty_Type4, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern7, overlooking=over_looking4, floor=floor4, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينة نصر فيلا رباعية بمساحة 500 م2 ومساحة مباني 400 م2 المرحلة غير متاح النموذج ( 0 ) بحري تطل على غير متاح عبارة عن 5 نوم 3 حمام تشطيب لوكس للإيجار قانون ج 12,000 ج ')
realty13.save()
realty13.tags.add(tag4)
realty13.save()

realtyImage13 = Photo(title=realty13.title, image='media/upload/realty13.jpeg', realty=realty13)
realtyImage13.save()


realty14 = Realty(city=city5, title=u"مدينتى فيلا 812 م2 بحري للبيع كاش على حديقة استلام فوري 5,000,000 ج ", typee=realty_Type4, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern8, overlooking=over_looking1, floor=floor5, interface=interface1,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=2, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا منفصلة بمساحة 812 م2 ومساحة مباني 323 م2 المرحلة الأولى النموذج ( X ) بحري تطل على حديقة عبارة عن 3 نوم 3 حمام بدون تشطيب للبيع كاش استلام فوري 5,000,000 ج للبيع كاش ')
realty14.save()
realty14.tags.add(tag6)
realty14.save()

realtyImage14 = Photo(title=realty14.title, image='media/upload/realty14.jpeg', realty=realty14)
realtyImage14.save()

realty15 = Realty(city=city5, title=u"مدينتى فيلا 400 م2 بحري للبيع كاش على حديقة صغيرة استلام فوري 3,500,000 ج ", typee=realty_Type4, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern8, overlooking=over_looking2, floor=floor6, interface=interface2,
                stage=realty_stage4,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا ثنائية بمساحة 400 م2 ومساحة مباني 275 م2 المرحلة الثانية النموذج ( H ) بحري و شرقي تطل على حديقة صغيرة عبارة عن 3 نوم 2 بلكونة 3 حمام تشطيب الشركة للبيع كاش استلام فوري 3,500,000 ج للبيع كاش ')
realty15.save()
realty15.tags.add(tag7)
realty15.save()

realtyImage15 = Photo(title=realty15.title, image='media/upload/realty15.jpeg', realty=realty15)
realtyImage15.save()


realty15 = Realty(city=city6, title=u"مدينتى فيلا 812 م2 بحري للبيع كاش على حديقة استلام فوري 5,000,000 ج ", typee=realty_Type2, size=140, garden_size=20, rooms_count=3,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern9, overlooking=over_looking3, floor=floor7, interface=interface2,
                stage=realty_stage4,
                price=5000000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا منفصلة بمساحة 812 م2 ومساحة مباني 323 م2 المرحلة الأولى النموذج ( X ) بحري تطل على حديقة عبارة عن 3 نوم 3 حمام بدون تشطيب للبيع كاش استلام فوري 5,000,000 ج للبيع كاش')
realty15.save()
realty15.tags.add(tag6)
realty15.save()

realtyImage15 = Photo(title=realty15.title, image='media/upload/realty15.jpeg', realty=realty15)
realtyImage15.save()

realty16 = Realty(city=city8, title=u"مدينتى فيلا 400 م2 بحري للبيع كاش على حديقة صغيرة استلام فوري 3,500,000 ج ", typee=realty_Type1, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern9, overlooking=over_looking4, floor=floor8, interface=interface1,
                stage=realty_stage3,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا ثنائية بمساحة 400 م2 ومساحة مباني 275 م2 المرحلة الثانية النموذج ( H ) بحري و شرقي تطل على حديقة صغيرة عبارة عن 3 نوم 2 بلكونة 3 حمام تشطيب الشركة للبيع كاش استلام فوري 3,500,000 ج للبيع كاش ')
realty16.save()
realty16.tags.add(tag5)
realty16.save()

realtyImage16 = Photo(title=realty16.title, image='media/upload/realty16.jpeg', realty=realty16)
realtyImage16.save()


realty17 = Realty(city=city8, title=u"مدينتى فيلا 380 م2 بحري للبيع كاش على حديقة استلام فوري 3,600,000 ج ", typee=realty_Type2, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern10, overlooking=over_looking2, floor=floor2, interface=interface1,
                stage=realty_stage6,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا ثنائية بمساحة 380 م2 ومساحة مباني 275 م2 المرحلة الثانية النموذج ( H ) بحري تطل على حديقة عبارة عن 3 نوم 3 حمام تشطيب الشركة للبيع كاش استلام فوري 3,600,000 ج للبيع كاش ')
realty17.save()
realty17.tags.add(tag2)
realty17.save()

realtyImage17 = Photo(title=realty17.title, image='media/upload/realty17.jpeg', realty=realty17)
realtyImage17.save()

realty18 = Realty(city=city9, title=u"العريش أرض 20250 م2 بحري للبيع كاش على بحيرات استلام فوري 5,500,000 ج ", typee=realty_Type4, size=120, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern12, overlooking=over_looking1, floor=floor5, interface=interface1,
                stage=realty_stage6,
                price=256000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'العريش أرض 20250 م2 بحري للبيع كاش على بحيرات استلام فوري 5,500,000 ج ')
realty18.save() 
realty18.tags.add(tag3)
realty18.save()

realtyImage18 = Photo(title=realty18.title, image='media/upload/realty18.jpeg', realty=realty18)
realtyImage18.save()


realty19 = Realty(city=city10, title=u"مدينتى فيلا 650 م2 بحري للبيع كاش على بحيرات استلام فوري 5,500,000 ج ", typee=realty_Type4, size=650, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern14, overlooking=over_looking1, floor=floor5, interface=interface1,
                stage=realty_stage6,
                price=5500000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا منفصلة بمساحة 650 م2 ومساحة مباني 400 م2 المرحلة الثانية النموذج ( E ) بحري تطل على بحيرات عبارة عن 3 نوم 3 حمام بدون تشطيب للبيع كاش استلام فوري 5,500,000 ج للبيع كاش ')
realty19.save()
realty19.tags.add(tag3)
realty19.save()

realtyImage19 = Photo(title=realty19.title, image='media/upload/realty19.jpg', realty=realty19)
realtyImage19.save()


realty20 = Realty(city=city10, title=u"مدينتى فيلا 1135 م2 بحري للبيع كاش على حديقة كبيرة استلام فوري 7,000,000 ج", typee=realty_Type4, size=1135, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern13, overlooking=over_looking1, floor=floor5, interface=interface1,
                stage=realty_stage6,
                price=7000000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا منفصلة بمساحة 1135 م2 ومساحة مباني 629 م2 المرحلة الثانية النموذج ( B ) بحري و شرقي و غربي تطل على حديقة كبيرة عبارة عن 4 نوم 4 بلكونة 4 حمام تشطيب الشركة للبيع كاش استلام فوري 7,000,000 ج للبيع كاش ')
realty20.save() 
realty20.tags.add(tag1)
realty20.save()

realtyImage20 = Photo(title=realty20.title, image='media/upload/realty20.jpg', realty=realty20)
realtyImage20.save()

realty21 = Realty(city=city10, title=u"مدينتى فيلا 392 م2 بحري للبيع بالتقسيط على حديقة كبيرة بمقدم 1,250,000 ج ", typee=realty_Type4, size=392, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern14, overlooking=over_looking1, floor=floor5, interface=interface1,
                stage=realty_stage6,
                price=1250000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بمدينتى فيلا ثنائية بمساحة 392 م2 ومساحة مباني 259 م2 بحديقة 151 م2 المرحلة الرابعة النموذج ( J ) بحري و قبلي تطل على حديقة كبيرة عبارة عن 3 نوم 3 حمام تشطيب الشركة بمقدم 1,250,000 ج ')
realty21.save() 
realty21.tags.add(tag4)
realty21.save()

realtyImage21 = Photo(title=realty21.title, image='media/upload/realty21.jpg', realty=realty21)
realtyImage21.save()

realty22 = Realty(city=city1, title=u"الرحاب شقة 131 م2 للبيع كاش تشطيب خاص استلام فوري 1,050,000 ج ", typee=realty_Type2, size=131, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern13, overlooking=over_looking2, floor=floor1, interface=interface1,
                stage=realty_stage6,
                price=1050000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بالرحاب شقة بمساحة 131 م2 بالطابق الثاني المرحلة الثامنة النموذج ( B ) غربي تطل على شارع عبارة عن 3 نوم 1 حمام تشطيب خاص للبيع كاش استلام فوري 1,050,000 ج للبيع كاش ')
realty22.save() 
realty22.tags.add(tag3)
realty22.save()

realtyImage22 = Photo(title=realty22.title, image='media/upload/realty22.jpg', realty=realty22)
realtyImage22.save()

realty23 = Realty(city=city1, title=u"الرحاب شقة 112 م2 بحري للبيع بالتقسيط على حديقة كبيرة بمقدم 570,000 ج ", typee=realty_Type4, size=112, garden_size=40, rooms_count=2,
                bathrooms_count=4,
                blaconies_count=1, pattern=pattern12, overlooking=over_looking1, floor=floor5, interface=interface2,
                stage=realty_stage6,
                price=570000, visitor_type=2, visitor_price=275000, Finishing=False, Received=False,
                investors_bulletin=False, meta_description="realty4",
                purpose=1, comment="", meta_keywords='realty4',
                description=u'بالرحاب شقة بمساحة 112 م2 بالطابق الأرضي بحديقة 67 م2 المرحلة السابعة النموذج ( B2 ) بحري و شرقي تطل على حديقة كبيرة عبارة عن 3 نوم 2 حمام تشطيب الشركة بمقدم 570,000 ج ')
realty23.save() 
realty23.tags.add(tag1)
realty23.save()

realtyImage23 = Photo(title=realty23.title, image='media/upload/realty23.jpg', realty=realty23)
realtyImage23.save()

# //////////////////////////////////////

# insert footerEntry

footerEntry.objects.all().delete()

entry1 = footerEntry(entryTypee=realty_Type1, purpose=1, entryCity=city1, active=True)
entry1.save()
entry2 = footerEntry(entryTypee=realty_Type1, purpose=2, entryCity=city2, active=True)
entry2.save()
entry3 = footerEntry(entryTypee=realty_Type2, purpose=1, entryCity=city3, active=True)
entry3.save()
entry4 = footerEntry(entryTypee=realty_Type2, purpose=2, entryCity=city4, active=True)
entry4.save()
entry5 = footerEntry(entryTypee=realty_Type3, purpose=1, entryCity=city5, active=True)
entry5.save()
entry6 = footerEntry(entryTypee=realty_Type3, purpose=2, entryCity=city6, active=True)
entry6.save()
entry7 = footerEntry(entryTypee=realty_Type4, purpose=1, entryCity=city7, active=True)
entry7.save()
entry8 = footerEntry(entryTypee=realty_Type4, purpose=2, entryCity=city8, active=True)
entry8.save()
entry9 = footerEntry(entryTypee=realty_Type1, purpose=1, entryCity=city2, active=True)
entry9.save()
entry10 = footerEntry(entryTypee=realty_Type2, purpose=1, entryCity=city4, active=True)
entry10.save()
entry11 = footerEntry(entryTypee=realty_Type3, purpose=2, entryCity=city6, active=True)
entry11.save()
entry12 = footerEntry(entryTypee=realty_Type4, purpose=2, entryCity=city10, active=True)
entry12.save()
entry13 = footerEntry(entryTypee=realty_Type4, purpose=1, entryCity=city1, active=True)
entry13.save()
entry14 = footerEntry(entryTypee=realty_Type1, purpose=2, entryCity=city6, active=True)
entry14.save()
entry15 = footerEntry(entryTypee=realty_Type2, purpose=2, entryCity=city5, active=True)
entry15.save()
entry16 = footerEntry(entryTypee=realty_Type3, purpose=1, entryCity=city9, active=True)
entry16.save()
entry17 = footerEntry(entryTypee=realty_Type4, purpose=1, entryCity=city10, active=True)
entry17.save()
# //////////////////////////////////////
