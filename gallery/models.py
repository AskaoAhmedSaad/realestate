# -*- coding:utf-8 -*-
from django.db import models
from sliders.models import Slider
from realties.models import Realty
from maps.models import Map
from patterns.models import PatternFloor, Pattern
from PIL import Image
import os

# Create your models here.

class Photo(models.Model):
    title = models.CharField('عنوان الصورة' , max_length = 255)
    image = models.ImageField('الصورة', upload_to='media/upload/')
    slider = models.OneToOneField(Slider, null=True)
    realty = models.OneToOneField(Realty, null=True)
    maps = models.OneToOneField(Map, null=True)
    patternfloor = models.OneToOneField(PatternFloor, null=True)
    pattern = models.OneToOneField(Pattern, null=True)


    def __unicode__(self):
        return self.title

    def admin_image(self):
        return "<img src = '%s' width='64'> " % self.image.url
    admin_image.allow_tags = True


    def save(self, size=(600, 500)):
        
        if not self.id and not self.image:
            return
        
        super(Photo, self).save()
        
        pw = self.image.width
        ph = self.image.height
        nw = size[0]
        nh = size[1]
        
        # only do this if the image needs resizing
        if (pw, ph) != (nw, nh):
            filename = str(self.image.path)
            image = Image.open(filename)
            pr = float(pw) / float(ph)
            nr = float(nw) / float(nh)
            
            if pr > nr:
                # image aspect is wider than destination ratio
                tw = int(round(nh * pr))
                image = image.resize((tw, nh), Image.ANTIALIAS)
                l = int(round(( tw - nw ) / 2.0))
                image = image.crop((l, 0, l + nw, nh))
            elif pr < nr:
                # image aspect is taller than destination ratio
                th = int(round(nw / pr))
                image = image.resize((nw, th), Image.ANTIALIAS)
                t = int(round(( th - nh ) / 2.0))
                print((0, t, nw, t + nh))
                image = image.crop((0, t, nw, t + nh))
            else:
                # image aspect matches the destination ratio
                image = image.resize(size, Image.ANTIALIAS)
            # quality_val = 90
            image.save(filename)


