# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0002_remove_map_image'),
        ('gallery', '0002_auto_20151130_1948'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='maps',
            field=models.OneToOneField(null=True, to='maps.Map'),
        ),
    ]
