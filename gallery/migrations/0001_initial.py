# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realties', '0002_remove_realty_image'),
        ('sliders', '0002_auto_20151130_0113'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd8\xb9\xd9\x86\xd9\x88\xd8\xa7\xd9\x86 \xd8\xa7\xd9\x84\xd8\xb5\xd9\x88\xd8\xb1\xd8\xa9')),
                ('image', models.ImageField(upload_to=b'media/upload/', verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb5\xd9\x88\xd8\xb1\xd8\xa9')),
                ('realty', models.ForeignKey(to='realties.Realty', null=True)),
                ('slider', models.ForeignKey(to='sliders.Slider', null=True)),
            ],
        ),
    ]
