# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patterns', '0002_auto_20151203_0250'),
        ('gallery', '0003_photo_maps'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='pattern',
            field=models.OneToOneField(null=True, to='patterns.Pattern'),
        ),
        migrations.AddField(
            model_name='photo',
            name='patternfloor',
            field=models.OneToOneField(null=True, to='patterns.PatternFloor'),
        ),
    ]
