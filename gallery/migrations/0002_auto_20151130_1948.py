# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='realty',
            field=models.OneToOneField(null=True, to='realties.Realty'),
        ),
        migrations.AlterField(
            model_name='photo',
            name='slider',
            field=models.OneToOneField(null=True, to='sliders.Slider'),
        ),
    ]
