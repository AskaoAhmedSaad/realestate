from django.contrib import admin
from .models import *

# Register your models here.
class PhotoAdmin(admin.ModelAdmin):
    list_display = ['admin_image', 'title']
    # exclude = ('slider', 'maps', 'realty')

admin.site.register(Photo, PhotoAdmin)