# -*- coding:utf-8 -*-
from django.db import models
from django.utils import timezone


class PatternType(models.Model):
    name = models.CharField('الأسم', max_length=255)
    description = models.TextField('الوصف')
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['id']


class PatternTypeStage(models.Model):
    name = models.CharField('الأسم', max_length=255)
    description = models.TextField('الوصف')
    typee = models.ForeignKey(PatternType, null=True, on_delete=models.SET_NULL)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.name + ' - ' + self.typee.name

    class Meta:
        ordering = ['id']


class PatternFloor(models.Model):
    name = models.CharField('الاسم ', max_length=255)
    # image = models.ImageField('الصوره ', upload_to='media/upload/patternsFloor/', blank=True, null=True)
    patten = models.ForeignKey('Pattern', null=True, on_delete=models.SET_NULL)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.name


class Pattern(models.Model):
    name = models.CharField('الأسم', max_length=255)
    # image = models.ImageField('الصورة', upload_to='media/upload/patterns/', blank=True, null=True)
    typee = models.ForeignKey(PatternType, null=True, on_delete=models.SET_NULL)
    stage = models.ForeignKey(PatternTypeStage, null=True, on_delete=models.SET_NULL)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['id']
