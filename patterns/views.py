from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def list_pattern_types(request):
    if request.method == 'GET':
        pattern_types = PatternType.objects.all()
        types = PatternType.objects.all()
        stages = PatternTypeStage.objects.all()
        paginator = Paginator(pattern_types, 9)  # Show 4 contacts per page
        page = request.GET.get('page')
        try:
            pattern_types = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            pattern_types = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            pattern_types = paginator.page(paginator.num_pages)
        context = {
            'pattern_types': pattern_types,
            'types': types,
            'stages': stages
        }
        return render(request, 'patternTypes/list_pattern_types.html', context)
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def search(request):
    if request.method == 'GET':
        try:
            typee = request.GET['type']
        except:
            typee = None

        try:
            stage = request.GET['stage']
        except:
            stage = None

        patterns = Pattern.objects.all()
        if typee != '' and typee is not None:
            try:
                typee = PatternType.objects.get(name__icontains=typee)
            except:
                typee = None
            if typee:
                patterns = patterns.filter(typee=typee)

        if stage != '' and stage is not None:
            try:
                stage = PatternTypeStage.objects.get(name=stage)
            except:
                stage = None
            if stage:
                patterns = patterns.filter(stage=stage)
        paginator = Paginator(patterns, 9)  # Show 4 contacts per page
        page = request.GET.get('page')
        try:
            patterns = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            patterns = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            patterns = paginator.page(paginator.num_pages)
        template = 'patterns/searchPatterns.html'
        context = {'patterns': patterns}
        return render(request, template, context)
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def list_pattern_type_stages(request, pk):
    pattern_type_stages = PatternTypeStage.objects.filter(typee__id=pk)

    context = {
        'pattern_type_stages': pattern_type_stages,
    }
    return render(request, 'list_pattern_type_stages.html', context)


def view_pattern(request, pk):
    pattern = Pattern.objects.get(pk=pk)
    return render(request, 'view_pattern.html', {'pattern': pattern})


def view_pattern_type(request, pk):
    try:
        int(pk)
    except:
        return redirect('/')
    pattern_type = PatternType.objects.get(pk=pk)
    pattern_types = PatternType.objects.all()
    # if request.method == 'POST':
    #     if request.POST['type'] != '':
    #         try:
    #             realties = realties.filter(typee__name=request.POST['type'])
    #         except:
    #             realties = Realty.objects.all()
    # paginator = Paginator(patterns, 9)  # Show 4 contacts per page
    # page = request.GET.get('page')
    # try:
    #     patterns = paginator.page(page)
    # except PageNotAnInteger:
    #     # If page is not an integer, deliver first page.
    #     patterns = paginator.page(1)
    # except EmptyPage:
    #     # If page is out of range (e.g. 9999), deliver last page of results.
    #     patterns = paginator.page(paginator.num_pages)
    return render(request, 'patternTypes/view_pattern_type.html', {'pattern_type': pattern_type,
                                                                   'pattern_types': pattern_types})
