# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Pattern',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
                ('image', models.ImageField(upload_to=b'media/upload/patterns/', null=True, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb5\xd9\x88\xd8\xb1\xd8\xa9', blank=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='PatternFloor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa7\xd8\xb3\xd9\x85 ')),
                ('image', models.ImageField(upload_to=b'media/upload/patternsFloor/', null=True, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb5\xd9\x88\xd8\xb1\xd9\x87 ', blank=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('patten', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='patterns.Pattern', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PatternType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
                ('description', models.TextField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81')),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.CreateModel(
            name='PatternTypeStage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
                ('description', models.TextField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81')),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('typee', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='patterns.PatternType', null=True)),
            ],
            options={
                'ordering': ['id'],
            },
        ),
        migrations.AddField(
            model_name='pattern',
            name='stage',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='patterns.PatternTypeStage', null=True),
        ),
        migrations.AddField(
            model_name='pattern',
            name='typee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='patterns.PatternType', null=True),
        ),
    ]
