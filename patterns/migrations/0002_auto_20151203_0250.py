# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patterns', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pattern',
            name='image',
        ),
        migrations.RemoveField(
            model_name='patternfloor',
            name='image',
        ),
    ]
