from django.conf.urls import url
from patterns import views

urlpatterns = [
    url(r'list_pattern_types/$', views.list_pattern_types, name="list_pattern_types"),
    url(r'view_pattern_type/(?P<pk>\d+)/$', views.view_pattern_type, name="view_pattern_type"),

    url(r'list_pattern_type_stages/(?P<pk>\d+)/$', views.list_pattern_type_stages, name="list_pattern_type_stages"),
    url('search/$', views.search, name='search_patterns'),
    url(r'view_pattern/(?P<pk>\d+)/$', views.view_pattern, name="view_pattern"),
]
