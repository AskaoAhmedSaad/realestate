from PIL import Image
from gallery.models import Photo
import os
from realestate.settings import BASE_DIR

class Resize(object):

    def __init__(self, image_id):
        self.image = Photo.objects.get(pk=image_id).image
        try:
            self.img = Image.open(self.image.path)
        except IOError:
            self.img = None

    def resize(self, w, h):
        if self.img is None:
            return 'http://placehold.it/%sx%s' % (w, h)
        try:
            os.mkdir('%s/media/upload/' % os.path.join(BASE_DIR, "media"))
        except OSError:
            pass
        img_dir = '%s/media/upload/' % os.path.join(BASE_DIR, "media")
        filename = self.image.path.split('/')[-1]
        ext = filename.split('.')[-1]
        filename = '.'.join(filename.split('.')[:-1])

        src_image = '%s/%s.%s' % (
            img_dir, filename, ext
        )

        op = '%s/%s-%sx%s.%s' % (
            img_dir, filename, w, h, ext
        )

        if not os.path.isfile(op):
            img = self.img.resize((w, h), Image.ANTIALIAS)
            img.save(op)
        url = '.'.join(self.image.url.split('.')[:-1])
        url += "-%sx%s.%s" % (w, h, ext)

        # if src_image:
        #     os.remove(src_image)
        return url
