# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('realties', '0002_remove_realty_image'),
        ('sliders', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='slider',
            name='image',
        ),
        migrations.DeleteModel(
            name='Photo',
        ),
    ]
