# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd8\xb9\xd9\x86\xd9\x88\xd8\xa7\xd9\x86 \xd8\xa7\xd9\x84\xd8\xb5\xd9\x88\xd8\xb1\xd8\xa9')),
                ('image', models.ImageField(upload_to=b'media/upload/', verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb5\xd9\x88\xd8\xb1\xd8\xa9')),
            ],
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb9\xd9\x86\xd9\x88\xd8\xa7\xd9\x86')),
                ('url', models.URLField(max_length=300, null=True, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb1\xd8\xa7\xd8\xa8\xd8\xb7 ', blank=True)),
                ('active', models.BooleanField(default=False, verbose_name=b'\xd9\x85\xd8\xaa\xd8\xa7\xd8\xad\xd9\x87 ')),
                ('description', models.TextField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81')),
                ('created', models.DateTimeField(default=django.utils.timezone.now)),
                ('image', models.OneToOneField(related_name='slider_image', null=True, to='sliders.Photo')),
            ],
        ),
    ]
