from django.template import Library, Node, Variable, VariableDoesNotExist
from sliders.resize import Resize
register = Library()


class ViewNode(Node):

    def __init__(self, img_id, width, height):
        self.id = img_id
        self.width = width
        self.height = height

    def render(self, context):
        self.id = Variable(str(self.id))
        try:
            imgid = self.id.resolve(context)
        except VariableDoesNotExist:
            return "http://placehold.it/%sx%s" % (self.width, self.height)
        r = Resize(imgid).resize(int(self.width), int(self.height))
        return r


def do_view(parser, token):
    x, img_id, width, height = token.split_contents()
    return ViewNode(img_id, width, height)

register.tag('resize', do_view)
