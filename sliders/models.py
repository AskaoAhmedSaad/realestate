# -*- coding:utf-8 -*-
from django.db import models
from django.utils import timezone
from sliders.models import *
from PIL import Image

# Create your models here.


class SliderQueryset(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)


class SliderManager(models.Manager):
    def get_queryset(self):
        return SliderQueryset(self.model, using=self._db)

    def all(self):
        return self.get_queryset().active()


class Slider(models.Model):
    title = models.CharField('العنوان', max_length=255)
    url = models.URLField('الرابط ', max_length=300, null=True, blank=True)
    active = models.BooleanField('متاحه ', default=False)
    description = models.TextField('الوصف')
    created = models.DateTimeField(default=timezone.now)
    objects = SliderManager()

    def __unicode__(self):
        return self.title
