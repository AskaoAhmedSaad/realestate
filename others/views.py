from django.shortcuts import render
from realties.models import *
from maps.models import MapType
from patterns.models import *
from others.models import *

# Create your views here.

def about_us(request):
    return render(request, 'about.html')


def contact_us(request):
    return render(request, 'contact.html')


def navbar(request):
    urgents = Realty.objects.filter(investors_bulletin=True)
    all_cities_list = [] # cities in dropdown without cities display in navbar
    navbarCities = City.objects.filter(in_navbar=True).order_by('id')[:5]
    patternTypes = PatternType.objects.all()
    all_cities = City.objects.all().order_by('id')
    for c in all_cities:
        if c in navbarCities:
            pass
        else:
            all_cities_list.append(c)
    maptypes = MapType.objects.all()
    return render(request, 'blocks/navbar.html',
        {'navbarCities':navbarCities, 'maptypes':maptypes,'patternTypes':patternTypes,
        'all_cities_list':all_cities_list, 'urgents':urgents})

