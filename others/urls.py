from django.conf.urls import url
from others import views

urlpatterns = [
    url(r'^about-us', views.about_us, name='about_us'),
    url(r'^contact-us', views.contact_us, name='contact_us'),
]
