# -*- coding:utf-8 -*-
from django.db import models


class Tag(models.Model):
    name = models.CharField('الأسم', max_length=255)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['id']

