from django import template
# from decimal import *

register = template.Library()

@register.filter(name='amountFilter')
def amountFilter(value):
	val1 = ('%f' % value).rstrip('0').rstrip('.')
	return val1
