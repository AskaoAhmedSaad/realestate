from django import template

register = template.Library()


def remove_dots(value, arg):
	value = value.replace(arg, ' ')
	return value

register.filter('remove_dots', remove_dots)
