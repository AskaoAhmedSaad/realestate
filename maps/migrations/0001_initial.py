# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Map',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
                ('image', models.ImageField(upload_to=b'media/upload/maps/', verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb5\xd9\x88\xd8\xb1\xd8\xa9')),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='MapType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
                ('description', models.TextField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81')),
            ],
        ),
        migrations.CreateModel(
            name='MapTypeStage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa3\xd8\xb3\xd9\x85')),
                ('description', models.TextField(verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81')),
                ('timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated', models.DateTimeField(auto_now=True, null=True)),
                ('typee', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='maps.MapType', null=True)),
            ],
        ),
        migrations.AddField(
            model_name='map',
            name='stage',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='maps.MapTypeStage', null=True),
        ),
        migrations.AddField(
            model_name='map',
            name='typee',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='maps.MapType', null=True),
        ),
    ]
