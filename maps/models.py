# -*- coding:utf-8 -*-
from django.db import models
from django.utils import timezone


class MapType(models.Model):
    name = models.CharField('الأسم', max_length=255)
    description = models.TextField('الوصف')

    def __unicode__(self):
        return self.name


class MapTypeStage(models.Model):
    name = models.CharField('الأسم', max_length=255)
    description = models.TextField('الوصف')
    typee = models.ForeignKey(MapType, null=True, on_delete=models.SET_NULL)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.name + ' - ' + self.typee.name



class Map(models.Model):
    name = models.CharField('الأسم', max_length=255)
    # image = models.ImageField('الصورة', upload_to='media/upload/maps/')
    typee = models.ForeignKey(MapType, null=True, on_delete=models.SET_NULL)
    stage = models.ForeignKey(MapTypeStage, null=True, on_delete=models.SET_NULL)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False, null=True)
    updated = models.DateTimeField(auto_now_add=False, auto_now=True, null=True)

    def __unicode__(self):
        return self.name
