from django.conf.urls import include, url

urlpatterns = [
    url(r'all_maps/$', 'maps.views.home', name='index_maps'),
    url(r'show/(?P<pk>\d+)/$', 'maps.views.show', name='show_map'),
    url(r'search/$', 'maps.views.search', name='search_maps'),
    url(r'view_maptype/(?P<pk>\d+)/$', 'maps.views.view_maptype', name="view_maptype"),
]
