from django.contrib import admin
from maps.models import *
from gallery.models import Photo
from django import forms
from django.utils.safestring import mark_safe

class ModelImageWidget(forms.Widget):
    def __init__(self, obj, attrs=None):
        self.object = obj
        super(ModelImageWidget, self).__init__(attrs)

    def render(self, name, value, attrs=None):
        if self.object.pk:
            return mark_safe(
                u'<a href="/media/%s" target="_blank"><img src="/media/%s" width=70 height=70/></a>' 
                % (self.object.path, self.object.path)
            )
        else:
            return mark_safe(u'')


class ImageForm(forms.ModelForm):
    image = forms.CharField(label='Image', required=False)

    def __init__(self, *args, **kwargs):
        super(ImageForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget = ModelImageWidget(self.instance)


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 1
    max_num = 1
    exclude = ('slider', 'title', 'realty', 'patternfloor', 'pattern')



class MapAdmin(admin.ModelAdmin):
    inlines = [PhotoInline]
    list_per_page = 30
    search_fields = ['name']
    ordering = ['-id']


admin.site.register(MapType)
admin.site.register(MapTypeStage)
admin.site.register(Map, MapAdmin)
