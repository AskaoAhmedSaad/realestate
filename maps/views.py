from django.shortcuts import render, HttpResponseRedirect, redirect
from .models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def view_maptype(request, pk):
    try:
        maptype = MapType.objects.get(pk=pk)
        allmaptypes = MapType.objects.all()
        context = {
            'maptype': maptype,
            'allmaptypes': allmaptypes
        }
        template = 'view_maptype.html'
        return render(request, template, context)
    except MapType.DoesNotExist:
        return redirect('/')


# Create your views here.
def home(request):
    types = MapType.objects.all()
    paginator = Paginator(types,2)  # Show 4 contacts per page
    page = request.GET.get('page')
    try:
        types = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        types = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        types = paginator.page(paginator.num_pages)
    template = 'index_maps.html'
    context = {
        'types': types
    }
    return render(request, template, context)


def show(request, pk):
    mapp = Map.objects.get(pk=pk)
    context = {
        'map': mapp
    }
    template = 'show_map.html'
    return render(request, template, context)


def search(request):
    if request.method == 'GET':
        try:
            name = request.GET['name']
        except:
            name = None
        try:
            typee = request.GET['type']
        except:
            typee - None
        try:
            stage = request.GET['stage']
        except:
            stage = None
        maps = Map.objects.all()
        if name != '' and name is not None:
            maps = maps.filter(name=name)
        if typee != '' and typee is not None:
            try:
                typee = MapType.objects.get(name=typee)
            except:
                typee = None
            if typee:
                maps = maps.filter(typee=typee)
        if stage != '' and stage is not None:
            try:
                stage = MapTypeStage.objects.get(name=stage)
            except:
                stage = None
            if stage:
                maps = maps.filter(stage=stage)
        context = {
            'maps': maps
        }
        template = 'search_maps.html'
        return render(request, template, context)
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
